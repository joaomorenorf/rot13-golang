package main

import (
	"io"
	"os"
)

func caesarCipher(buffer []byte, caesarKey int) byte {
	if buffer[0] >= byte('a') && buffer[0] <= byte('z') {
		return (buffer[0]-byte('a')+byte(caesarKey))%26 + byte('a')
	} else if buffer[0] >= byte('A') && buffer[0] <= byte('Z') {
		return (buffer[0]-byte('A')+byte(caesarKey))%26 + byte('A')
	} else {
		return buffer[0]
	}
}

func readData(file *os.File, buffer []byte) int {
	bRead, err := file.Read(buffer)
	if bRead != 0 {
		checkErr(err)
	}
	_, err = file.Seek(-int64(bRead), io.SeekCurrent)
	checkErr(err)
	return bRead
}

func writeData(file *os.File, buffer []byte) {
	_, err := file.Write(buffer)
	checkErr(err)
}
func main() {
	caesarKey := 13
	// Opening a file in read-write
	file, err := os.OpenFile("never.txt", os.O_RDWR, 0755)
	checkErr(err)
	buffer := make([]byte, 1)
	for {
		// Read data and move seeker
		if readData(file, buffer) == 0 {
			break
		}

		// Cipher data
		buffer[0] = caesarCipher(buffer, caesarKey)

		// Write ciphered data
		writeData(file, buffer)
	}
}

// print if error
func checkErr(e error) {
	if e != nil {
		panic(e)
	}
}
